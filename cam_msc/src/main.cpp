#include <Arduino.h>
#include <sys/dirent.h>
#include "usb/usb_host.h"
#include "usb/msc_host_vfs.h"
#include "esp_camera.h"

#define MNT_PATH         "/usb"
#define APP_QUIT_PIN     GPIO_NUM_0
msc_host_device_handle_t msc_device = nullptr;
msc_host_vfs_handle_t vfs_handle = nullptr;
static QueueHandle_t app_queue;

extern "C" {
typedef struct {
    enum {
        APP_QUIT, // Signals request to exit the application
        APP_DEVICE_CONNECTED, // USB device connect event
        APP_DEVICE_DISCONNECTED, // USB device disconnect event
    } id;

    union {
        uint8_t new_dev_address; // Address of new USB device for APP_DEVICE_CONNECTED event if
    } data;
} app_message_t;
}

static void gpio_cb(void *arg)
{
    BaseType_t xTaskWoken = pdFALSE;
    constexpr app_message_t message = {
        .id = app_message_t::APP_QUIT,
    };

    if (app_queue) {
        xQueueSendFromISR(app_queue, &message, &xTaskWoken);
    }

    if (xTaskWoken == pdTRUE) {
        portYIELD_FROM_ISR();
    }
}

static void msc_event_cb(const msc_host_event_t *event, void *arg)
{
    if (event->event == msc_host_event_t::MSC_DEVICE_CONNECTED) {
        log_i("MSC device connected");
        app_message_t message = {
            app_message_t::APP_DEVICE_CONNECTED,
            event->device.address,
        };
        xQueueSend(app_queue, &message, portMAX_DELAY);
        return;
    }
    if (event->event == msc_host_event_t::MSC_DEVICE_DISCONNECTED) {
        log_i("MSC device disconnected");
        constexpr app_message_t message = {
            app_message_t::APP_DEVICE_DISCONNECTED,
        };
        xQueueSend(app_queue, &message, portMAX_DELAY);
    }
}

void print_device_info(msc_host_device_info_t *info)
{
    constexpr size_t megabyte = 1024 * 1024;
    const uint64_t capacity = (static_cast<uint64_t>(info->sector_size) * info->sector_count) / megabyte;

    printf("Device info:\n");
    printf("\t Capacity: %llu MB\n", capacity);
    printf("\t Sector size: %" PRIu32 "\n", info->sector_size);
    printf("\t Sector count: %" PRIu32 "\n", info->sector_count);
    printf("\t PID: 0x%04X \n", info->idProduct);
    printf("\t VID: 0x%04X \n", info->idVendor);
    printf("\t iProduct: %ls \n", info->iProduct);
    printf("\t iManufacturer: %ls \n", info->iManufacturer);
    printf("\t iSerialNumber: %ls \n", info->iSerialNumber);
}

void task_pic()
{
    const auto directory = "/usb/esp";
    struct stat s = {0};
    const bool directory_exists = stat(directory, &s) == 0;
    if (!directory_exists) {
        if (mkdir(directory, 0775) != 0) {
            log_e("mkdir failed with errno: %s", strerror(errno));
        }
    }
    camera_fb_t *fb = nullptr;
#define FILE_PATH_MAX_LEN 256 // 假设最长的文件路径不会超过256个字符

    while (true) {
        char fileDir[FILE_PATH_MAX_LEN];
        fb = esp_camera_fb_get();
        if (!fb) {
            log_e("Camera capture failed");
            continue;
        }
        const int64_t time = fb->timestamp.tv_sec * 1000LL + fb->timestamp.tv_usec / 1000LL; // 计算时间戳为毫秒
        snprintf(fileDir, sizeof(fileDir), "/usb/esp/%lld.jpg", time);
        FILE *f = fopen(fileDir, "wb+");
        if (f == nullptr) {
            log_e("Failed to open file for writing %s", strerror(errno));
            esp_camera_fb_return(fb);
            return;
        }
        setvbuf(f, nullptr, _IOFBF, fb->len);
        if (fwrite(fb->buf, fb->len, 1, f) == 0) {
            fclose(f);
            esp_camera_fb_return(fb);
            return;
        }
        fclose(f);
        esp_camera_fb_return(fb);
    }
}


void usb_task(void *args)
{
    constexpr usb_host_config_t host_config = {.intr_flags = ESP_INTR_FLAG_LEVEL1};
    ESP_ERROR_CHECK(usb_host_install(&host_config));

    constexpr msc_host_driver_config_t msc_config = {
        .create_backround_task = true,
        .task_priority = 5,
        .stack_size = 4096,
        .callback = msc_event_cb,
    };
    ESP_ERROR_CHECK(msc_host_install(&msc_config));

    while (true) {
        uint32_t event_flags;
        usb_host_lib_handle_events(portMAX_DELAY, &event_flags);

        if (event_flags & USB_HOST_LIB_EVENT_FLAGS_NO_CLIENTS) {
            if (usb_host_device_free_all() == ESP_OK) {
                break;
            };
        }
        if (event_flags & USB_HOST_LIB_EVENT_FLAGS_ALL_FREE) {
            break;
        }
    }

    vTaskDelay(10); // Give clients some time to uninstall
    log_i("Deinitializing USB");
    ESP_ERROR_CHECK(usb_host_uninstall());
    vTaskDelete(nullptr);
}

void setup()
{
    camera_config_t config;
    config.ledc_channel = LEDC_CHANNEL_0;
    config.ledc_timer = LEDC_TIMER_0;
    config.pin_d0 = Y2_GPIO_NUM;
    config.pin_d1 = Y3_GPIO_NUM;
    config.pin_d2 = Y4_GPIO_NUM;
    config.pin_d3 = Y5_GPIO_NUM;
    config.pin_d4 = Y6_GPIO_NUM;
    config.pin_d5 = Y7_GPIO_NUM;
    config.pin_d6 = Y8_GPIO_NUM;
    config.pin_d7 = Y9_GPIO_NUM;
    config.pin_xclk = XCLK_GPIO_NUM;
    config.pin_pclk = PCLK_GPIO_NUM;
    config.pin_vsync = VSYNC_GPIO_NUM;
    config.pin_href = HREF_GPIO_NUM;
    config.pin_sccb_sda = SIOD_GPIO_NUM;
    config.pin_sccb_scl = SIOC_GPIO_NUM;
    config.pin_pwdn = PWDN_GPIO_NUM;
    config.pin_reset = RESET_GPIO_NUM;
    config.xclk_freq_hz = 20000000;
    config.pixel_format = PIXFORMAT_JPEG;

    if (psramFound()) {
        config.frame_size = FRAMESIZE_QVGA;
        config.jpeg_quality = 10;
        config.fb_count = 2;
    } else {
        config.frame_size = FRAMESIZE_QVGA;
        config.jpeg_quality = 12;
        config.fb_count = 1;
    }

    // camera init
    esp_err_t err = esp_camera_init(&config);
    if (err != ESP_OK) {
        log_i("Camera init failed with error 0x%x", err);
        delay(3000);
        ESP.restart();
        return;
    }
    log_i("get sensor ");
    sensor_t *s = esp_camera_sensor_get();
    // drop down frame size for higher initial frame rate
    s->set_framesize(s, FRAMESIZE_SVGA);
    // Create FreeRTOS primitives
    app_queue = xQueueCreate(5, sizeof(app_message_t));
    assert(app_queue);

    const BaseType_t task_created = xTaskCreate(usb_task, "usb_task", 4096, NULL, 2, NULL);
    assert(task_created);

    // Init BOOT button: Pressing the button simulates app request to exit
    // It will disconnect the USB device and uninstall the MSC driver and USB Host Lib
    const gpio_config_t input_pin = {
        .pin_bit_mask = BIT64(APP_QUIT_PIN),
        .mode = GPIO_MODE_INPUT,
        .pull_up_en = GPIO_PULLUP_ENABLE,
        .intr_type = GPIO_INTR_NEGEDGE,
    };
    ESP_ERROR_CHECK(gpio_config(&input_pin));
    ESP_ERROR_CHECK(gpio_install_isr_service(ESP_INTR_FLAG_LEVEL1));
    ESP_ERROR_CHECK(gpio_isr_handler_add(APP_QUIT_PIN, gpio_cb, nullptr));

    log_i("Waiting for USB flash drive to be connected");
}

void loop()
{
    app_message_t msg;
    xQueueReceive(app_queue, &msg, portMAX_DELAY);

    if (msg.id == app_message_t::APP_DEVICE_CONNECTED) {
        ESP_ERROR_CHECK(msc_host_install_device(msg.data.new_dev_address, &msc_device));
        const esp_vfs_fat_mount_config_t mount_config = {
            .format_if_mount_failed = false,
            .max_files = 3,
            .allocation_unit_size = 8192,
        };
        ESP_ERROR_CHECK(msc_host_vfs_register(msc_device, MNT_PATH, &mount_config, &vfs_handle));

        msc_host_device_info_t info;
        ESP_ERROR_CHECK(msc_host_get_device_info(msc_device, &info));
        msc_host_print_descriptors(msc_device);
        print_device_info(&info);

        log_i("ls command output:");
        struct dirent *d;
        DIR *dh = opendir(MNT_PATH);
        assert(dh);
        while ((d = readdir(dh)) != NULL) {
            printf("%s\n", d->d_name);
        }
        closedir(dh);
        task_pic();
    }
    if ((msg.id == app_message_t::APP_DEVICE_DISCONNECTED) || (msg.id == app_message_t::APP_QUIT)) {
        if (vfs_handle) {
            ESP_ERROR_CHECK(msc_host_vfs_unregister(vfs_handle));
            vfs_handle = NULL;
        }
        if (msc_device) {
            ESP_ERROR_CHECK(msc_host_uninstall_device(msc_device));
            msc_device = NULL;
        }
        if (msg.id == app_message_t::APP_QUIT) {
            // This will cause the usb_task to exit
            ESP_ERROR_CHECK(msc_host_uninstall());
        }
    }
}
