
#include <SPIFFS.h>
#include <ESPmDNS.h>

#include <utility>
#include "flash_fs_web_server.h"


uint8_t buff[WEB_FILE_BUFFER_SIZE];

LevelUri::LevelUri(const char *uri) : Uri(uri) {}

LevelUri::LevelUri(const String &uri) : Uri(uri) {}

bool LevelUri::canHandle(const String &requestUri, std::vector<String> &pathArgs)
{
    return requestUri.startsWith(_uri) || _uri == requestUri;
}

Uri *LevelUri::clone() const
{
    return new LevelUri(_uri);
}

void FlashFsWebServer::begin(const String &mdnsHost, uint16_t port)
{
    SPIFFS.begin();
    this->mdnsHost_ = mdnsHost;
    if (MDNS.begin(mdnsHost)) {
        log_i("MDNS responder started");
    }
    onNotFound([this]() {
        handleNotFound();
    });
    WebServer::begin(port);
    const char *mdns_cstr = mdnsHost.c_str();
    log_i("no proxy for *.local \n"
          "pc -> [http://%s] or [http://%s.local]\n"
          "mobile ->[http://%s.local]", mdns_cstr, mdns_cstr, mdns_cstr);
    const auto &localIp = WiFi.localIP();
    log_i("http://%u.%u.%u.%u",
          localIp[0], localIp[1], localIp[2], localIp[3]);

}

void FlashFsWebServer::onAssetsDir(const Uri &uri_)
{
    on(uri_, [this]() {
        handleFile(uri().c_str());
    });
}

void FlashFsWebServer::onFile(const Uri &uri, const String &path)
{
    on(uri, [this, path]() {
        handleFile(path);
    });
}

String FlashFsWebServer::getContentType(const String &filename)
{
    if (hasArg("download")) {
        return "application/octet-stream";
    }
    if (filename.endsWith(".htm")) {
        return "text/html";
    }
    if (filename.endsWith(".html")) {
        return "text/html";
    }
    if (filename.endsWith(".css")) {
        return "text/css";
    }
    if (filename.endsWith(".js")) {
        return "application/javascript";
    }
    if (filename.endsWith(".png")) {
        return "image/png";
    }
    if (filename.endsWith(".gif")) {
        return "image/gif";
    }
    if (filename.endsWith(".jpg")) {
        return "image/jpeg";
    }
    if (filename.endsWith(".ico")) {
        return "image/x-icon";
    }
    if (filename.endsWith(".xml")) {
        return "text/xml";
    }
    if (filename.endsWith(".pdf")) {
        return "application/x-pdf";
    }
    if (filename.endsWith(".zip")) {
        return "application/x-zip";
    }
    if (filename.endsWith(".gz")) {
        return "application/x-gzip";
    }
    if (filename.endsWith(".svg")) {
        return "image/svg+xml";
    }
    return "text/plain";
}

void FlashFsWebServer::handleNotFound()
{
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += uri();
    message += "\nMethod: ";
    message += (method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += args();
    message += "\n";
    for (int i = 0; i < args(); i++) {
        message += " " + argName(i) + ": " + arg(i) + "n";
    }

    send(404, "text/plain", message);
}

void FlashFsWebServer::handleFile(const String &path)
{
    String filePath = gzipSrc_ ? path + ".gz" : path;
    File file = SPIFFS.open(filePath, FILE_READ);
    if (!file || file.size() == 0) {
        log_w("There was an error opening the file [%s] for reading", path.c_str());
        handleNotFound();
        return;
    }
    size_t length = file.size();

    auto wiFiClient = this->client();
    wiFiClient.print("HTTP/1.1 200 OK\n");
    wiFiClient.print("Content-Type: ");
    wiFiClient.print(getContentType(String(path)));
    wiFiClient.print("\nContent-Length: ");
    wiFiClient.print(length);
    if (gzipSrc_) {
        wiFiClient.print("\nContent-Encoding: gzip");
    }
    wiFiClient.print("\nConnection: close\n\n");

    size_t count = length / WEB_FILE_BUFFER_SIZE;
    for (int i = 0; i < count; i++) {
        file.read(buff, WEB_FILE_BUFFER_SIZE);
        wiFiClient.write(buff, WEB_FILE_BUFFER_SIZE);
    }
    size_t lastLen = length % WEB_FILE_BUFFER_SIZE;
    if (lastLen) {
        file.read(buff, lastLen);
        wiFiClient.write(buff, lastLen);
    }
    wiFiClient.flush();
    file.close();
}

void FlashFsWebServer::onUriBraces(const char *uri_, WebServer::THandlerFunction fn)
{
    on(UriBraces(uri_), std::move(fn));
}

void FlashFsWebServer::fileMapping(const std::list<UriSrc> &urlFileMap)
{
    for (const auto &item: urlFileMap) {
        onFile(item.uri, item.path);
    }
}

void FlashFsWebServer::setSrcGzip(bool isGzipSrc)
{
    this->gzipSrc_ = isGzipSrc;
}
