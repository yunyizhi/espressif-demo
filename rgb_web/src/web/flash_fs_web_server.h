
#ifndef FLASH_FS_WEB_SERVER_H
#define FLASH_FS_WEB_SERVER_H

#include <Uri.h>
#include <uri/UriBraces.h>
#include <WebServer.h>
#include <list>

class LevelUri : public Uri {
public:
    explicit LevelUri(const char *uri);

    explicit LevelUri(const String &uri);

    bool canHandle(const String &requestUri, std::vector<String> &pathArgs) override;

    Uri *clone() const override;
};

struct UriSrc {
    String uri;
    String path;
};

#define WEB_FILE_BUFFER_SIZE  (50 * 1024)

class FlashFsWebServer : public WebServer {
public:
    void begin(const String &mdnsHost, uint16_t port = 80);

    void onAssetsDir(const Uri &uri_);

    void onFile(const Uri &uri, const String &path);

    void fileMapping(const std::list<UriSrc> &urlFileMap);

    void onUriBraces(const char *uri_, WebServer::THandlerFunction fn);

    /**
     * 当资源文件是经过gzip压缩之后的，会追加.gz后缀去读文件，
     * 在响应头加 Content-Encoding: gzip
     * */
    void setSrcGzip(bool isGzipSrc);

private:
    String mdnsHost_{""};
    bool gzipSrc_ = false;

    String getContentType(const String &filename);

    void handleFile(const String &path);

    void handleNotFound();
};


#endif // FLASH_FS_WEB_SERVER_H
