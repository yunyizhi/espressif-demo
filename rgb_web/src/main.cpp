#include <Arduino.h>
#include "rgb/rmt_rgb.h"
#include "web/flash_fs_web_server.h"

const char *wifiSsid = WIFI_SSID;
const char *wifiPasswd = WIFI_PASSWORD;

#define RGB_LED_NUM 24

RmtRgb rmtRgb;
FlashFsWebServer flashFsWebServer;

void connectWifi(const char *ssid, const char *passphrase)
{
    WiFiClass::mode(WIFI_STA);
    WiFi.begin(ssid, passphrase);
    log_i("connecting to router... ");
    // 等待wifi连接成功 连不上把调试日志打开
    while (WiFiClass::status() != WL_CONNECTED) {
        log_printf(".");
        delay(500);
    }
    const IPAddress &localIp = WiFi.localIP();
    const IPAddress &gatewayIp = WiFi.gatewayIP();
    log_i("\nWiFi connected, local IP address:%u.%u.%u.%u\nWifi GWip is:%u.%u.%u.%u\n",
          localIp[0], localIp[1], localIp[2], localIp[3],
          gatewayIp[0], gatewayIp[1], gatewayIp[2], gatewayIp[3]);
}

void setRgbColor()
{
    String pinStr = flashFsWebServer.pathArg(0);
    String rgb = flashFsWebServer.pathArg(1);
    if (!pinStr || !rgb) {
        return;
    }
    int pin = pinStr.toInt();
    uint32_t rgbVal = rgb.toInt();
    rmtRgb.setColor(pin, rgbVal);
    rmtRgb.send();
    log_i("set led %d rgb:%u", pin, rgbVal);
    flashFsWebServer.send(200, "text/plain", "ok");
}

void getRgbNumber()
{
    flashFsWebServer.sendHeader("Access-Control-Allow-Origin", "*");
    flashFsWebServer.send(200, "application/json", String("{\"count\":") + RGB_LED_NUM + "}");
}

void setup()
{
    // 4 号引脚 绑定 24个灯
    if (!rmtRgb.begin(GPIO_NUM_4, RGB_LED_NUM)) {
        log_e("Failed to initialize RMT");
        delay(10000);
        ESP.restart();
    }
    connectWifi(wifiSsid, wifiPasswd);
    flashFsWebServer.onAssetsDir(LevelUri("/assets"));
    flashFsWebServer.fileMapping({
                                         {"/",            "/index.html"},
                                         {"/index.html",  "/index.html"},
                                         {"/favicon.ico", "/favicon.ico"}
                                 });
    flashFsWebServer.onUriBraces("/setRgbColor/{}/{}", setRgbColor);
    flashFsWebServer.on("/getRgbNumber", getRgbNumber);
    //flashFsWebServer.setSrcGzip(true);
    flashFsWebServer.begin("esp32rgb");

}

void loop()
{
    flashFsWebServer.handleClient();
}