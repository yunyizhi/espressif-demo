# esp32 web rmt rgb

一个通过web控制rgb灯板颜色的项目。

## 需要配置nodejs和npm

## 添加了platformio的target

| 标题                   | target名称       | 作用                                              |
|----------------------|----------------|-------------------------------------------------|
| Build Vue            | buildWeb       | 构建web项目，并复制到当前目录的data目录                         |
| Build And Upload Web | buildUploadWeb | 构建web项目，并复制到当前目录的data目录， 构建文件系统，并上传文件系统         |
| Upload All           | uploadAll      | 构建web项目，并复制到当前目录的data目录， 构建文件系统，并上传文件系统，并编译程序上传 |

### 一键烧录
`pio run -t uploadAll`

或者在platformio任务树上找到Upload All 双击。

## 编译说明
* 修改platformio.ini connection下的wifi 连接信息
```ini
[connection]
```
* 修改main.cpp rgb灯数量和引脚

## 注意事项

需要较好的供电，供电功率要求较大，同时波纹可能对rgb造成错误信号。