import './assets/main.css'
import Vue3ColorPicker from "vue3-colorpicker";
import "vue3-colorpicker/style.css";
import {createApp} from 'vue'
import App from './App.vue'
import {Col, Row, Cell, CellGroup , Space } from 'vant';


createApp(App)
    .use(Vue3ColorPicker)
    .use(Col)
    .use(Row)
    .use(Cell)
    .use(CellGroup)
    .use(Space)
    .mount('#app')
