
#include "rmt_rgb.h"

bool RmtRgb::begin(uint8_t pin, uint32_t ledNums) {
    if (ledData_) {
        log_w("current obj has initialized");
        return true;
    }
    this->ledNums_ = ledNums;
    this->allRgbBits_ = ledNums * 24;
    this->ledData_ = new rmt_data_t[allRgbBits_];
    log_i("init rmt tx");

    if ((rmtSend_ = rmtInit(pin, RMT_TX_MODE, RMT_MEM_64)) == nullptr) {
        log_e("RMT init failed");
        rmtSend_ = nullptr;
        return false;
    }
    rmtSetTick(rmtSend_, 100); // 每个tick 0.1us

    log_i("rmt initialized");
    return true;
}

void RmtRgb::setColor(int ledIndex, uint8_t red, uint8_t green, uint8_t blue) {
    const uint8_t grbArr[] = {green, red, blue};
    encode(ledIndex, grbArr);
}

void RmtRgb::setAllColor(uint32_t color) {
    for (int i = 0; i < ledNums_; ++i) {
        setColor(i, color);
    }
}

void RmtRgb::setColor(int ledIndex, uint32_t color) {
    // rgb转grb
    uint8_t grbArr[] = {};
    grbArr[0] = (color >> 8) & 0xFF;
    grbArr[1] = (color >> 16) & 0xFF;
    grbArr[2] = color & 0xFF;
    encode(ledIndex, grbArr);
}

void RmtRgb::send() {
    rmtWriteBlocking(rmtSend_, ledData_, allRgbBits_);
}

void RmtRgb::encode(int ledIndex, const uint8_t *grbArr) {
    rmt_data_t *led_data_ptr = ledData_ + ledIndex * 24;
    for (int col = 0; col < 3; col++) {
        for (int bit = 0; bit < 8; bit++) {
            if ((grbArr[col] & (1 << (7 - bit)))) {
                // 高 0.8 低 0.4 为1
                led_data_ptr->level0 = 1;
                led_data_ptr->duration0 = 8; // 0.8us
                led_data_ptr->level1 = 0;
                led_data_ptr->duration1 = 4; // 0.4us
            } else {
                // 高 0.4 低 0.8 为 0
                led_data_ptr->level0 = 1;
                led_data_ptr->duration0 = 4; // 0.4us
                led_data_ptr->level1 = 0;
                led_data_ptr->duration1 = 8; // 0.8us
            }
            led_data_ptr++;
        }
    }
}

RmtRgb::~RmtRgb() {
    delete[] ledData_;
}


