
#ifndef RMT_RGB_RMT_RGB_H
#define RMT_RGB_RMT_RGB_H

#include <Arduino.h>

class RmtRgb {
public:
    virtual ~RmtRgb();

    bool begin(uint8_t pin, uint32_t ledNums);

    /**
     * @param ledIndex 没有检查  ledIndex 是否在 0 ~ RGB_NUMS -1 如果不能保证请自行检查
     * */
    void setColor(int ledIndex, uint8_t red, uint8_t green, uint8_t blue);

    /**
     * @param ledIndex 没有检查  ledIndex 是否在 0 ~ RGB_NUMS -1 如果不能保证请自行检查
     * */
    void setColor(int ledIndex, uint32_t color);

    void setAllColor(uint32_t color);

    void send();

private:
    rmt_obj_t *rmtSend_ = nullptr;
    rmt_data_t *ledData_ = nullptr;
    uint32_t ledNums_ = 0;
    uint32_t allRgbBits_ = 0;

    void encode(int ledIndex, const uint8_t *grbArr);


};


#endif // RMT_RGB_RMT_RGB_H
