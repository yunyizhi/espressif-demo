#include <Arduino.h>

#include "rmt_rgb.h"

#define RGB_LED_NUMS 24

RmtRgb rmtRgb;

const uint32_t color_list[]{
        0xFF0000, 0x00FF00, 0x0000FF,
};

const int delay_time_count = 60;
uint32_t delay_times[delay_time_count] = {50, 40, 30, 20, 10, 8, 6, 3, 3, 3};

void setup() {
    if (!rmtRgb.begin(GPIO_NUM_47, RGB_LED_NUMS)) {
        log_e("init rmt failed! will restart in 3 sec.");
        delay(3000);
        ESP.restart();
    }
    // 填充后面剩余的 延时为1
    std::fill(delay_times + 10, delay_times + delay_time_count, 1);

    rmtRgb.setAllColor(0x00FF00);
    rmtRgb.send();
    log_i("set all on");
    delay(1000);
    rmtRgb.setAllColor(0);
    rmtRgb.send();
    log_i("set all off");

}


int delay_index = -1;

void loop() {

    for (const auto &color: color_list) {
        int last_index = RGB_LED_NUMS - 1;
        delay_index = ++delay_index % delay_time_count;
        for (int i = 0; i < RGB_LED_NUMS; ++i) {
            rmtRgb.setColor(last_index, 0);
            rmtRgb.setColor(i, color);
            rmtRgb.send();
            last_index = i;
            delay(delay_times[delay_index]);
        }
    }

}