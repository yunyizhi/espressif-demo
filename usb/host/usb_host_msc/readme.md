# usb host msc
esp32作为主机读写U盘或其他MSC设备。

>如果报错建议将U盘格式化为FAT32试试，如果不报错可以不管。

本例程使用支持otg芯片的esp32系列 esp32s2/esp32s3 
esp32s3使用deckit-c造型的esp32s3。有部分的OTG需要焊盘解锁，但基本上都能使用。
esp32s2使用的是 esp32 s2 mini 验证。

### 编译说明

这里预置了两个环境配置，`esp32-s3-devkitc-1`和`lolin_s2_mini`

选择对应环境进行烧录。如果是使用非` esp32 s2 mini`

需要将  `board_upload.after_reset = no_reset`去掉

board_build.extra_flags中的`-D ARDUINO_LOLIN_S2_MINI`去掉



### s2 mini使用说明
s2 mini这个板子比较便宜，但串口0未引出。
这里需要使用唯一的usb口子也就是otg会用到的口子下载。

##### 下载

otg 无法进入自动进入下载模式，需要按住io0上的按钮之后rst一次，使得s2上电时检测io0为低，以下载模式启动，就可以松开手。

下载完成之后使用 VBUS和GND以 5V供电。

##### 查看日志

查看日志需要USB转TTL模块，比如CH340G USB转TTL模块, 串口0未引出，本例子 TX为39，RX为37，使用的variants文件夹下对应的默认引脚。
因而需要使用这两个引脚查看串口打印。由于串口0未引出，这个两个引脚是启动后设置的，
因而看不到esp-rom打印的上电日志是正常的。接上串口之后按rst看看是否有日志打印，来确定是否接反。

```text
[   789][I][msc_example_idf.c:262] start(): [example] Waiting for USB flash drive to be connected
```

