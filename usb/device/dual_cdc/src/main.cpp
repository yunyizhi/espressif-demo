#include <Arduino.h>
#include "USB.h"
USBCDC cdc0(0);
USBCDC cdc1(1);

void setup() {
    log_i("init uart");
    cdc0.begin(115200);
    log_i("init cdc0");
    cdc1.begin(115200);
    log_i("init cdc1");
    USB.begin();
    // write your initialization code here
}

void loop() {

    cdc0.println("cdc 0");
    cdc1.println("cdc 1");
    delay(1000);
}