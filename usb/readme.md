# USB

主要包含esp32s2 esp32s3等含OTG外设的系列，相关示例。

主要补充host相关例程，包括HID MSC UVC CDC等。其在espidf下有相关例程，这里给出使用platformio下 arduino-esp32移植的版本。



### host主机模式

esp32s2/esp32s3作为主机驱动对应的设备。

* HID HOST

  当前包含驱动 鼠标键盘

* MSC HOST 

  驱动U盘

* UVC HOST

  驱动USB 摄像头 这里使用usb stream组件，仅仅支持mjpeg

* CDC ACM HOST 

  驱动usb转串口芯片

### device 模式

* DUAL CDC

  otg虚拟两个串口， 在主机上显示两个独立串口设备。