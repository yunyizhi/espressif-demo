#include <esp_lcd_panel_ops.h>
#include <esp_heap_caps.h>
#include <esp_log.h>
#include <driver/jpeg_decode.h>
#include "sdkconfig.h"
#include "p4_ek79007_display.h"
#include "city_jpeg.h"

static const char *TAG = "p4_jpeg_mipi";

void show_jpeg(const bsp_lcd_handles_t *lcd_handles)
{
    jpeg_decoder_handle_t jpgd_handle;

    jpeg_decode_engine_cfg_t decode_eng_cfg = {
            .timeout_ms = 40,
    };

    ESP_ERROR_CHECK(jpeg_new_decoder_engine(&decode_eng_cfg, &jpgd_handle));

    jpeg_decode_cfg_t decode_cfg_rgb = {
#if CONFIG_BSP_LCD_COLOR_FORMAT_RGB888
            .output_format = JPEG_DECODE_OUT_FORMAT_RGB888,
#else
            .output_format = JPEG_DECODE_OUT_FORMAT_RGB565,
#endif
            .rgb_order = JPEG_DEC_RGB_ELEMENT_ORDER_BGR,
    };
    jpeg_decode_picture_info_t header_info;
    size_t size_jpeg = sizeof city;
    ESP_ERROR_CHECK(jpeg_decoder_get_info(city, size_jpeg, &header_info));
    ESP_LOGI(TAG, "header parsed, width is %" PRId32 ", height is %" PRId32, header_info.width, header_info.height);
    uint32_t out_size = 0;
    jpeg_decode_memory_alloc_cfg_t rx_mem_cfg = {
            .buffer_direction = JPEG_DEC_ALLOC_OUTPUT_BUFFER,
    };
    size_t rx_buffer_size = 0;
    // 防止超过文件头真实大小
#if CONFIG_BSP_LCD_COLOR_FORMAT_RGB888
    const size_t output_bytes = header_info.width * header_info.height * 3 * 120 / 100;
#else
    const size_t output_bytes = header_info.width * header_info.height * 2 * 120 / 100;
#endif
    uint8_t *decode_result_buff = (uint8_t *) jpeg_alloc_decoder_mem(output_bytes,&rx_mem_cfg, &rx_buffer_size);
    ESP_ERROR_CHECK(jpeg_decoder_process(jpgd_handle, &decode_cfg_rgb, city, size_jpeg, decode_result_buff, rx_buffer_size,
                                 &out_size));
    esp_lcd_panel_draw_bitmap(lcd_handles->panel, 0, 0, header_info.width, header_info.height, decode_result_buff);
}

void app_main(void)
{
    bsp_display_brightness_init();
    bsp_lcd_handles_t lcd_panels;
    bsp_display_new_with_handles(&lcd_panels);
    show_jpeg(&lcd_panels);
    bsp_display_backlight_on();
}
