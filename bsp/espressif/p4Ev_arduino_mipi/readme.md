# ESP32-P4-Function-EV-Board（工程样品版） 使用mipi dsi和jpeg外设Arduino例程



参考 [ESP32-P4-Function-EV-Board - - — esp-dev-kits latest 文档 (espressif.com)](https://docs.espressif.com/projects/esp-dev-kits/zh_CN/latest/esp32p4/esp32-p4-function-ev-board/user_guide.html)

这里购买的版本搭配的屏幕驱动为`ek79007`

分辨率为1024 * 600 暂未做成menuconfig，使用代码硬编码。当前例程会使用esp32p4的jpeg硬件解码并显示到mipi屏幕上。

该屏幕支持rgb888需要通过menuconfig配置 `BSP_LCD_COLOR_FORMAT`。

> 这里提供一个分区表，如果需要使用请使用menuconfig配置。





