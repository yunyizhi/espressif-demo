# ESP32-P4-Function-EV-Board（工程样品版）使用以太网例程

参考 [ESP32-P4-Function-EV-Board - - — esp-dev-kits latest 文档 (espressif.com)](https://docs.espressif.com/projects/esp-dev-kits/zh_CN/latest/esp32p4/esp32-p4-function-ev-board/user_guide.html)

这里使用 IP101 引脚可参考上述文档中原理图
```ini
CONFIG_EXAMPLE_ETH_MDC_GPIO=31
CONFIG_EXAMPLE_ETH_MDIO_GPIO=52
CONFIG_EXAMPLE_ETH_PHY_RST_GPIO=51
```
