//
// Created by immor on 2025/2/8.
//

#include "DateTool.h"

void DateTool::configNtp(const int timezone, const int daylightOffset_sec, const char *server1, const char *server2,
    const char *server3)
{
    configTime(timezone, daylightOffset_sec, server1, server2, server3);
    log_i("Waiting for NTP time sync: ");
    time_t nowSecs = time(nullptr);
    while (nowSecs < 8 * 3600 * 2) {
        delay(500);
        log_printf(".");
        yield();
        nowSecs = time(nullptr);
    }
    tm timeInfo{};
    gmtime_r(&nowSecs, &timeInfo);
    syncOnce = true;
    log_i("Current time: %s", getDateTime().c_str());
}

String DateTool::getDateTime() const
{
    if (!syncOnce) {
        log_w("The time is not synchronized");
    }
    time_t now;
    tm timeinfo{};
    time(&now);
    localtime_r(&now, &timeinfo);
    char strftime_buf[20];
    strftime(strftime_buf, sizeof(strftime_buf), "%Y-%m-%d %H:%M:%S", &timeinfo);
    return {strftime_buf};
}

tm DateTool::getTime() const
{
    if (!syncOnce) {
        log_w("The time is not synchronized");
    }
    time_t now;
    tm timeinfo{};
    time(&now);
    localtime_r(&now, &timeinfo);
    return timeinfo;
}
