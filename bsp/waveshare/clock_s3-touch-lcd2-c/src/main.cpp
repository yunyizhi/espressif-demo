#include <Arduino.h>
#include <Arduino_GFX_Library.h>
#include "DateTool.h"
#include <WiFi.h>

#define EXAMPLE_PIN_NUM_LCD_SCLK 39
#define EXAMPLE_PIN_NUM_LCD_MOSI 38
#define EXAMPLE_PIN_NUM_LCD_MISO 40
#define EXAMPLE_PIN_NUM_LCD_DC 42
#define EXAMPLE_PIN_NUM_LCD_RST (-1)
#define EXAMPLE_PIN_NUM_LCD_CS 45
#define EXAMPLE_PIN_NUM_LCD_BL 1

#define EXAMPLE_LCD_ROTATION 0
#define EXAMPLE_LCD_H_RES 240
#define EXAMPLE_LCD_V_RES 320

const char *ssid = WIFI_SSID;
const char *password = WIFI_PASSWORD;
/* More data bus class: https://github.com/moononournation/Arduino_GFX/wiki/Data-Bus-Class */
Arduino_DataBus *bus = new Arduino_ESP32SPI(
    EXAMPLE_PIN_NUM_LCD_DC /* DC */, EXAMPLE_PIN_NUM_LCD_CS /* CS */,
    EXAMPLE_PIN_NUM_LCD_SCLK /* SCK */, EXAMPLE_PIN_NUM_LCD_MOSI /* MOSI */, EXAMPLE_PIN_NUM_LCD_MISO /* MISO */);

/* More display class: https://github.com/moononournation/Arduino_GFX/wiki/Display-Class */
Arduino_GFX *gfx = new Arduino_ST7789(
    bus, EXAMPLE_PIN_NUM_LCD_RST /* RST */, EXAMPLE_LCD_ROTATION /* rotation */, true /* IPS */,
    EXAMPLE_LCD_H_RES /* width */, EXAMPLE_LCD_V_RES /* height */);


#define BACKGROUND RGB565_BLACK
#define MARK_COLOR RGB565_WHITE
#define SUBMARK_COLOR RGB565_DARKGREY // RGB565_LIGHTGREY
#define HOUR_COLOR RGB565_WHITE
#define MINUTE_COLOR RGB565_BLUE // RGB565_LIGHTGREY
#define SECOND_COLOR RGB565_RED

#define SIXTIETH 0.016666667
#define TWELFTH 0.08333333
#define SIXTIETH_RADIAN 0.10471976
#define TWELFTH_RADIAN 0.52359878
#define RIGHT_ANGLE_RADIAN 1.5707963

static int16_t w, h, center;
static int16_t hHandLen, mHandLen, sHandLen, markLen;
static float sdeg, mdeg, hdeg;
static int16_t osx = 0, osy = 0, omx = 0, omy = 0, ohx = 0, ohy = 0; // Saved H, M, S x & y coords
static int16_t nsx, nsy, nmx, nmy, nhx, nhy; // H, M, S x & y coords
static int16_t hh, mm, ss;

constexpr long gmtOffset_sec = 28800; // 中国时区偏移量，UTC+8，即28800秒
constexpr int daylightOffset_sec = 0; // 夏令时偏移，中国不使用夏令时，设为0
const char *ntpServer = "ntp.aliyun.com";

static int16_t *cached_points;

void draw_round_clock_mark(int16_t innerR1, int16_t outerR1, int16_t innerR2, int16_t outerR2, int16_t innerR3,
                           int16_t outerR3);

void draw_and_erase_cached_line(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t color, int16_t *cache,
                                int16_t cache_len, bool cross_check_second, bool cross_check_hour);

void redraw_hands_cached_draw_and_erase();

void write_cache_pixel(int16_t x, int16_t y, int16_t color, bool cross_check_second, bool cross_check_hour);

DateTool date_tool;
void connectWifi()
{
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    // wait for WiFi connection
    log_i("Waiting for WiFi to connect...");
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        log_printf(".");
    }
    log_i(" connected");
}


void setup(void)
{
    // Serial.setDebugOutput(true);
    // while(!Serial);
    log_i("Arduino_GFX Clock example");

    // Init Display
    if (!gfx->begin()) {
        log_i("gfx->begin() failed!");
    }
    gfx->fillScreen(BACKGROUND);

#ifdef EXAMPLE_PIN_NUM_LCD_BL
    pinMode(EXAMPLE_PIN_NUM_LCD_BL, OUTPUT);
    digitalWrite(EXAMPLE_PIN_NUM_LCD_BL, HIGH);
#endif

    // init LCD constant
    w = gfx->width();
    h = gfx->height();
    if (w < h) {
        center = w / 2;
    } else {
        center = h / 2;
    }
    hHandLen = center * 3 / 8;
    mHandLen = center * 2 / 3;
    sHandLen = center * 5 / 6;
    markLen = sHandLen / 6;
    cached_points = (int16_t *) malloc((hHandLen + 1 + mHandLen + 1 + sHandLen + 1) * 2 * 2);
    gfx->setTextSize(2);
    gfx->setTextColor(RGB565(0,139,139), RGB565_BLACK);
    // Draw 60 clock marks
    draw_round_clock_mark(
        // draw_square_clock_mark(
        center - markLen, center,
        center - (markLen * 2 / 3), center,
        center - (markLen / 2), center);
    connectWifi();
    date_tool.configNtp(gmtOffset_sec, daylightOffset_sec, ntpServer);

}

void loop()
{
    auto time = date_tool.getTime();
    ss = time.tm_sec;
    mm = time.tm_min;
    hh = time.tm_hour;

    // Pre-compute hand degrees, x & y coords for a fast screen update
    sdeg = SIXTIETH_RADIAN * ss;
    nsx = cos(sdeg - RIGHT_ANGLE_RADIAN) * sHandLen + center;
    nsy = sin(sdeg - RIGHT_ANGLE_RADIAN) * sHandLen + center;
    if ((nsx != osx) || (nsy != osy)) {
        mdeg = (SIXTIETH * sdeg) + (SIXTIETH_RADIAN * mm); // 0-59 (includes seconds)
        hdeg = (TWELFTH * mdeg) + (TWELFTH_RADIAN * hh); // 0-11 (includes minutes)
        mdeg -= RIGHT_ANGLE_RADIAN;
        hdeg -= RIGHT_ANGLE_RADIAN;
        nmx = cos(mdeg) * mHandLen + center;
        nmy = sin(mdeg) * mHandLen + center;
        nhx = cos(hdeg) * hHandLen + center;
        nhy = sin(hdeg) * hHandLen + center;

        // redraw hands
        redraw_hands_cached_draw_and_erase();
        gfx->setCursor(0, 280);

        gfx->print(date_tool.getDateTime());
        ohx = nhx;
        ohy = nhy;
        omx = nmx;
        omy = nmy;
        osx = nsx;
        osy = nsy;

        delay(1);
    }
}

void draw_round_clock_mark(int16_t innerR1, int16_t outerR1, int16_t innerR2, int16_t outerR2, int16_t innerR3,
                           int16_t outerR3)
{
    float x, y;
    int16_t x0, x1, y0, y1, innerR, outerR;
    uint16_t c;

    for (uint8_t i = 0; i < 60; i++) {
        if ((i % 15) == 0) {
            innerR = innerR1;
            outerR = outerR1;
            c = MARK_COLOR;
        } else if ((i % 5) == 0) {
            innerR = innerR2;
            outerR = outerR2;
            c = MARK_COLOR;
        } else {
            innerR = innerR3;
            outerR = outerR3;
            c = SUBMARK_COLOR;
        }

        mdeg = (SIXTIETH_RADIAN * i) - RIGHT_ANGLE_RADIAN;
        x = cos(mdeg);
        y = sin(mdeg);
        x0 = x * outerR + center;
        y0 = y * outerR + center;
        x1 = x * innerR + center;
        y1 = y * innerR + center;

        gfx->drawLine(x0, y0, x1, y1, c);
    }
}

void draw_square_clock_mark(int16_t innerR1, int16_t outerR1, int16_t innerR2, int16_t outerR2, int16_t innerR3,
                            int16_t outerR3)
{
    float x, y;
    int16_t x0, x1, y0, y1, innerR, outerR;
    uint16_t c;

    for (uint8_t i = 0; i < 60; i++) {
        if ((i % 15) == 0) {
            innerR = innerR1;
            outerR = outerR1;
            c = MARK_COLOR;
        } else if ((i % 5) == 0) {
            innerR = innerR2;
            outerR = outerR2;
            c = MARK_COLOR;
        } else {
            innerR = innerR3;
            outerR = outerR3;
            c = SUBMARK_COLOR;
        }

        if ((i >= 53) || (i < 8)) {
            x = tan(SIXTIETH_RADIAN * i);
            x0 = center + (x * outerR);
            y0 = center + (1 - outerR);
            x1 = center + (x * innerR);
            y1 = center + (1 - innerR);
        } else if (i < 23) {
            y = tan((SIXTIETH_RADIAN * i) - RIGHT_ANGLE_RADIAN);
            x0 = center + (outerR);
            y0 = center + (y * outerR);
            x1 = center + (innerR);
            y1 = center + (y * innerR);
        } else if (i < 38) {
            x = tan(SIXTIETH_RADIAN * i);
            x0 = center - (x * outerR);
            y0 = center + (outerR);
            x1 = center - (x * innerR);
            y1 = center + (innerR);
        } else if (i < 53) {
            y = tan((SIXTIETH_RADIAN * i) - RIGHT_ANGLE_RADIAN);
            x0 = center + (1 - outerR);
            y0 = center - (y * outerR);
            x1 = center + (1 - innerR);
            y1 = center - (y * innerR);
        }
        gfx->drawLine(x0, y0, x1, y1, c);
    }
}

void redraw_hands_cached_draw_and_erase()
{
    gfx->startWrite();
    draw_and_erase_cached_line(center, center, nsx, nsy, SECOND_COLOR, cached_points, sHandLen + 1, false, false);
    draw_and_erase_cached_line(center, center, nhx, nhy, HOUR_COLOR, cached_points + ((sHandLen + 1) * 2), hHandLen + 1,
                               true, false);
    draw_and_erase_cached_line(center, center, nmx, nmy, MINUTE_COLOR,
                               cached_points + ((sHandLen + 1 + hHandLen + 1) * 2), mHandLen + 1, true, true);
    gfx->endWrite();
}

void draw_and_erase_cached_line(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t color, int16_t *cache,
                                int16_t cache_len, bool cross_check_second, bool cross_check_hour)
{
    bool steep = _diff(y1, y0) > _diff(x1, x0);
    if (steep) {
        _swap_int16_t(x0, y0);
        _swap_int16_t(x1, y1);
    }

    int16_t dx, dy;
    dx = _diff(x1, x0);
    dy = _diff(y1, y0);

    int16_t err = dx / 2;
    int8_t xstep = (x0 < x1) ? 1 : -1;
    int8_t ystep = (y0 < y1) ? 1 : -1;
    x1 += xstep;
    int16_t x, y, ox, oy;
    for (uint16_t i = 0; i <= dx; i++) {
        if (steep) {
            x = y0;
            y = x0;
        } else {
            x = x0;
            y = y0;
        }
        ox = *(cache + (i * 2));
        oy = *(cache + (i * 2) + 1);
        if ((x == ox) && (y == oy)) {
            if (cross_check_second || cross_check_hour) {
                write_cache_pixel(x, y, color, cross_check_second, cross_check_hour);
            }
        } else {
            write_cache_pixel(x, y, color, cross_check_second, cross_check_hour);
            if ((ox > 0) || (oy > 0)) {
                write_cache_pixel(ox, oy, BACKGROUND, cross_check_second, cross_check_hour);
            }
            *(cache + (i * 2)) = x;
            *(cache + (i * 2) + 1) = y;
        }
        if (err < dy) {
            y0 += ystep;
            err += dx;
        }
        err -= dy;
        x0 += xstep;
    }
    for (uint16_t i = dx + 1; i < cache_len; i++) {
        ox = *(cache + (i * 2));
        oy = *(cache + (i * 2) + 1);
        if ((ox > 0) || (oy > 0)) {
            write_cache_pixel(ox, oy, BACKGROUND, cross_check_second, cross_check_hour);
        }
        *(cache + (i * 2)) = 0;
        *(cache + (i * 2) + 1) = 0;
    }
}

void write_cache_pixel(int16_t x, int16_t y, int16_t color, bool cross_check_second, bool cross_check_hour)
{
    int16_t *cache = cached_points;
    if (cross_check_second) {
        for (uint16_t i = 0; i <= sHandLen; i++) {
            if ((x == *(cache++)) && (y == *(cache))) {
                return;
            }
            cache++;
        }
    }
    if (cross_check_hour) {
        cache = cached_points + ((sHandLen + 1) * 2);
        for (uint16_t i = 0; i <= hHandLen; i++) {
            if ((x == *(cache++)) && (y == *(cache))) {
                return;
            }
            cache++;
        }
    }
    gfx->writePixel(x, y, color);
}
