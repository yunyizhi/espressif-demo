# esp32s3 双摄例程
esp32s3驱动dvp接口摄像头和usb摄像头,这里使用两个ffplay分别播放。



## 硬件

#### usb摄像头

这里使用乐鑫官方店的usb摄像头，其他厂商的不一定支持，本人购买过其他摄像头，虽然也支持
目标分辨率和编码格式，但驱动并未适配好，无法使用本例程。

可能存在官方店很贵的情况，可能是没货了，其他商家也有同款。

#### 开发板

这里需要OTG功能，需要s3或者s2。

这里用到psram缓存帧。usb 摄像头驱动部分已经固定为使用psram申请帧，请使用带psram的s2/s3。

这里仅测试了esp32 s3 cam。Freenove ESP32-S3-WROOM Board 某宝叫做 ESP32-S3 WROOM N16R8 CAM

[Freenove/Freenove_ESP32_S3_WROOM_Board: Apply to FNK0085 (github.com)](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2FFreenove%2FFreenove_ESP32_S3_WROOM_Board)



### 编译说明

修改platformio.ini的connection选项，指定wifi ssid和密码 以及服务端ip,这里是运行ffmpeg的主机的ip.

```ini
[connection]
wifi_ssid = \"test0\"
wifi_password = \"12345687\"
host = \"192.168.0.121\"
```



## ffmpeg

### 安装ffmpeg

在widows下可以从该地址下载安装[Builds - CODEX FFMPEG @ gyan.dev](https://www.gyan.dev/ffmpeg/builds/)

这里我下载的是最新的release版本。

linux下可以使用对应具体系统的包管理安装如apt yum。

### 播放视频

#### DVP摄像头播放

```shell
ffplay -f mjpeg -i "udp://0.0.0.0:8004" -bufsize 1024 -fflags +genpts -analyzeduration 0 -probesize 32  -noautoexit
```

#### USB接口摄像头播放

```shell
ffplay -f mjpeg -i "udp://0.0.0.0:8005" -bufsize 1024 -fflags +genpts -analyzeduration 0 -probesize 32  -
noautoexit
```

也可以使用ffmpeg转 rtp再使用vlc播放

可以参考根目录下的`ffmpeg` 文件夹下的`video`项目操作方法


## 其他附加

### ffmpeg录制esp32视频

```bash
ffmpeg -re -f mjpeg -i udp://0.0.0.0:8004 -c:v libx264 -preset medium -crf 23 output.mp4
```

`-crf 23`是一个常见的恒定速率因子值，用于平衡文件大小和质量。较低的CRF值意味着更高的质量和更大的文件大小。

