#include <Arduino.h>
#include <driver/rmt.h>

#define SERVO_GPIO_NUM 14 // 舵机信号线

static rmt_obj_t *rmt_send = nullptr;

bool setup_servo(uint8_t pin)
{
    log_i("init rmt tx");

    if ((rmt_send = rmtInit(pin, RMT_TX_MODE, RMT_MEM_64)) == nullptr) {
        log_e("RMT init failed");
        rmt_send = nullptr;
        return false;
    }
    rmtSetTick(rmt_send, 1000); // 分频到1M Hz 也就是每个tick 1us

    log_i("rmt initialized");
    return true;
}

/**
 * @param angle 0 ~ 180 这里的精度为 1 度
 * */
void send_servo_pulse(int angle)
{
    if (angle < 0) {
        angle = 0;
    }
    if (angle > 180) {
        angle = 180;
    }

    int pulse_width_us = 500 + ((angle * 2000) / 180); // 0 ~ 180 -> 500 ~ 2500

    rmt_data_t pwm[3];
    for (auto &i: pwm) {
        i.duration0 = pulse_width_us; // 高电平脉宽
        i.level0 = 1;
        i.duration1 = 20000 - pulse_width_us; // 低电平脉宽 周期 - 高电平
        i.level1 = 0;
    }

    rmtWriteBlocking(rmt_send, pwm, 3);

}

/**
 * @param angle 0 ~ 1800 表示 0.0 ~ 180.0 精度为 0.1 度
 * */
void send_servo_pulse_with_precision(int angle)
{
    if (angle < 0) {
        angle = 0;
    }
    if (angle > 1800) {
        angle = 1800;
    }

    int pulse_width_us = 500 + ((angle * 2000) / 1800); // 0 ~ 1800 -> 500 ~ 2500

    rmt_data_t pwm[3];
    for (auto &i: pwm) {
        i.duration0 = pulse_width_us; // 高电平脉宽
        i.level0 = 1;
        i.duration1 = 20000 - pulse_width_us; // 低电平脉宽 周期 - 高电平
        i.level1 = 0;
    }

    rmtWriteBlocking(rmt_send, pwm, 3);
}

void setup()
{
    bool setupServo = setup_servo(SERVO_GPIO_NUM);
    if (!setupServo) {
        log_e("will reboot in 3 sec ");
        delay(3000);
        ESP.restart();
    }
}

void loop()
{
    send_servo_pulse(0);
    delay(1000);
    send_servo_pulse(90);
    delay(1000);
    send_servo_pulse(180);
    delay(1000);
    // 0.1 度的分辨率 0.5度步进
//    for (int i = 0; i < 1800; i+=5) {
//        send_servo_pulse_with_precision(i);
//        delay(1);
//    }

}

