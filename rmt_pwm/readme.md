# rmt pwm

使用esp32的 rmt外设生成pwm，用于驱动舵机

rmt可参考 [红外遥控 (RMT) - ESP32-S3 - — ESP-IDF 编程指南 v5.3.1 文档](https://docs.espressif.com/projects/esp-idf/zh_CN/v5.3.1/esp32s3/api-reference/peripherals/rmt.html#rmt)  或者技术参考手册



* 分频参数 1000   
  ` rmtSetTick(rmt_send, 1000);`
  表示 1000ns

这里默认是80Mhz时钟 每个tick为 12.5ns 将其修改为 1us

* 舵机驱动
  脉宽 大多数舵机 是 500us ~ 2500 us对应 0 ~ 180 度

频率为50hz，这里发送三个周期为 20ms的波形

接线

| 舵机         | esp32以及电源 |   
|------------|-----------|
| 信号线 黄色     | GPIO14    | 
| V+ 红色      | 5V        |   
| GND 棕色或者黑色 | GND       |   





