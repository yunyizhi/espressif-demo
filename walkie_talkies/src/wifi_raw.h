
#include <Arduino.h>
#include <esp_wifi.h>

#ifndef WALKIE_TALKIES_WIFI_RAW_H
#define WALKIE_TALKIES_WIFI_RAW_H

constexpr uint8_t WLAN_IEEE_HEADER_AIR2GROUND[] =
        {
                0x08, 0x01, 0x00, 0x00,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0x11, 0x22, 0x33, 0x44, 0x55, 0x66,
                0x11, 0x22, 0x33, 0x44, 0x55, 0x66,
                0x10, 0x86
        };

constexpr size_t WLAN_IEEE_HEADER_SIZE = sizeof(WLAN_IEEE_HEADER_AIR2GROUND);
constexpr size_t WLAN_MAX_PACKET_SIZE = 1500;

uint8_t wifi_tx_buffer[WLAN_MAX_PACKET_SIZE];

constexpr uint16_t audio_pack_size = 512;

struct PcmPack {
    uint8_t data[audio_pack_size];
    uint16_t len;
};

constexpr uint16_t rx_buffer_queue_size = 4;
PcmPack rxBufferMsg[rx_buffer_queue_size] = {};

#endif //WALKIE_TALKIES_WIFI_RAW_H
