#include <Arduino.h>
#include "wifi_raw.h"
#include <esp_wifi.h>
#include <driver/i2s.h>
#include <esp_check.h>

static const char *TAG = "walkie_talkies";

static QueueHandle_t pcm_rx_group = nullptr;

static void wifi_rx_cb(void *buffer, wifi_promiscuous_pkt_type_t type);

static void audio_play_task(void *);

static void config_wifi_raw()
{

    wifi_init_config_t wifi_config = WIFI_INIT_CONFIG_DEFAULT();
    esp_err_t ret = esp_wifi_init(&wifi_config);
    if (ret != ESP_OK) {
        log_e("esp_wifi_init failed : %d", ret);
        delay(1000);
        ESP.restart();
    }
    esp_wifi_set_mode(WIFI_MODE_STA);
    esp_wifi_start();
    esp_err_t err = esp_wifi_config_80211_tx_rate(WIFI_IF_STA, WIFI_PHY_RATE_18M);
    if (err != ESP_OK) {
        log_e("esp_wifi_init failed : %d", ret);
        delay(1000);
        ESP.restart();
    }
    memcpy(wifi_tx_buffer, WLAN_IEEE_HEADER_AIR2GROUND, WLAN_IEEE_HEADER_SIZE);
    // 设置混杂模式接收回调函数
    esp_wifi_set_promiscuous_rx_cb(wifi_rx_cb);
    esp_wifi_set_promiscuous(true);
}

static esp_err_t config_speaker_micro_phone()
{
    i2s_config_t i2s_player_config = {
            .mode = (i2s_mode_t) (I2S_MODE_MASTER | I2S_MODE_TX),
            .sample_rate = CONFIG_SAMPLE_RATE,
            .bits_per_sample = static_cast<i2s_bits_per_sample_t>(CONFIG_SAMPLE_BITS),
            .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
            .communication_format = (i2s_comm_format_t) (I2S_COMM_FORMAT_STAND_I2S),
            .intr_alloc_flags = 0,
            .dma_buf_count = 10,
            .dma_buf_len = 512
    };

    i2s_config_t i2s_recoder_config = {
            .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_RX),
            .sample_rate = CONFIG_SAMPLE_RATE,
            .bits_per_sample = static_cast<i2s_bits_per_sample_t>(CONFIG_SAMPLE_BITS),
            .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
            .communication_format = (i2s_comm_format_t) (I2S_COMM_FORMAT_STAND_I2S),
            .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
            .dma_buf_count = 10,
            .dma_buf_len = 512
    };


    i2s_pin_config_t recoder_pin_cfg = {
            .bck_io_num = CONF_I2S_RECODER_BCLK,
            .ws_io_num = CONF_I2S_RECODER_WS,
            .data_out_num = I2S_PIN_NO_CHANGE,
            .data_in_num = CONF_I2S_RECODER_DIN,
    };

    i2s_pin_config_t player_pin_config = {
            .bck_io_num = CONF_I2S_PLAYER_BCLK,
            .ws_io_num = CONF_I2S_PLAYER_WS,
            .data_out_num = CONF_I2S_PLAYER_DOUT,
            .data_in_num = I2S_PIN_NO_CHANGE
    };
    ESP_RETURN_ON_ERROR(i2s_driver_install(I2S_NUM_0, &i2s_player_config, 0, nullptr), TAG,
                        "install player i2s failed");
    ESP_RETURN_ON_ERROR(i2s_set_pin(I2S_NUM_0, &player_pin_config), TAG, "set recoder i2s pins failed");

    ESP_RETURN_ON_ERROR(
            i2s_set_clk(I2S_NUM_0, CONFIG_SAMPLE_RATE, (i2s_bits_per_sample_t) CONFIG_SAMPLE_BITS, I2S_CHANNEL_MONO),
            TAG, "set player i2s clk failed");

    ESP_RETURN_ON_ERROR(i2s_driver_install(I2S_NUM_1, &i2s_recoder_config, 0, nullptr), TAG,
                        "install recoder i2s failed");
    ESP_RETURN_ON_ERROR(i2s_set_pin(I2S_NUM_1, &recoder_pin_cfg), TAG, "set recoder i2s pins failed");

    return ESP_OK;

}

void setup()
{
    esp_err_t ret = config_speaker_micro_phone();
    if (ret != ESP_OK) {
        log_e("config_speaker_micro_phone failed : %d", ret);
        delay(1000);
        ESP.restart();
    }
    pcm_rx_group = xQueueCreate(rx_buffer_queue_size + 1, sizeof(PcmPack));
    config_wifi_raw();
    xTaskCreatePinnedToCore(audio_play_task, "audioPlayTask", 1024 * 8,
                            nullptr, 1, nullptr, 0);
}


uint8_t *rec_buffer = wifi_tx_buffer + WLAN_IEEE_HEADER_SIZE;

size_t bytes_read = 0;

void loop()
{
    esp_err_t ret = i2s_read(I2S_NUM_0, rec_buffer, audio_pack_size, &bytes_read, portMAX_DELAY);

    if (bytes_read < 1) {
        log_e("Failed read! ret:%x", ret);
        return;
    }
    //  放大音量
    for (uint32_t i = 0; i < bytes_read; i += CONFIG_SAMPLE_BITS / 8) {
        (*(uint16_t *) (rec_buffer + i)) <<= CONFIG_VOLUME_GAIN;
    }
    esp_err_t result = esp_wifi_80211_tx(WIFI_IF_STA, wifi_tx_buffer, WLAN_IEEE_HEADER_SIZE + bytes_read, false);
    if (result != ESP_OK) {
        log_e("send failed %d", result);
    }
}

static int rx_queue_index = 0;

static void wifi_rx_cb(void *buffer, wifi_promiscuous_pkt_type_t type) {
    switch (type) {
        case WIFI_PKT_MGMT:
        case WIFI_PKT_CTRL:
        case WIFI_PKT_MISC:
            return;
        default:
            break;
    }
    auto *pkt = (wifi_promiscuous_pkt_t *) buffer;
    uint16_t len = pkt->rx_ctrl.sig_len;
    uint8_t *data = pkt->payload;
    if (memcmp(data + 10, WLAN_IEEE_HEADER_AIR2GROUND + 10, 6) != 0) {
        return;
    }

    data += WLAN_IEEE_HEADER_SIZE;
    len -= WLAN_IEEE_HEADER_SIZE; //skip the 802.11 header

    len -= 4; //the received length has 4 more bytes at the end for some reason.

    uint16_t packSize = std::min(len, audio_pack_size);
    PcmPack *pPack = rxBufferMsg + rx_queue_index;
    memcpy(pPack->data, data, packSize);
    pPack->len = packSize;

    xQueueSend(pcm_rx_group, pPack, 0);
    rx_queue_index = (rx_queue_index + 1) & (audio_pack_size - 1);
}

static void audio_play_task(void *)
{
    PcmPack receivedPack{};
    size_t bytes_written = 0;
    for (;;) {
        if (xQueueReceive(pcm_rx_group, &receivedPack, portMAX_DELAY) == pdTRUE) {
            i2s_write(I2S_NUM_0, receivedPack.data, receivedPack.len, &bytes_written, portMAX_DELAY);
            if (bytes_written < receivedPack.len) {
                log_w("not play all");
            }
        }
    }
}