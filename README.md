# espressif-demo

#### 介绍
esp32 esp8266 示例代码



### 项目

* [cam-udp-s3-tft](./cam-udp-s3-tft)  esp32-esp32 无线视频监控

  使用esp32cam采集图片发送给esp32s3通过tft显示

* [cam_lcd](./cam_lcd)  esp32使用spi屏幕 st7789显示画面

* [cam_rgb565_lcd](./cam_lcd)  esp32使用spi屏幕 st7789显示画面，但摄像头使用rgb565输出。

* [TestSPIFFS](./TestSPIFFS) SPIFFS读取入门，使用platformio一键将文件写入flash后从中读取。

  platformio esp32 项目data目录使用。 


* [bsp](./bsp) 部分成品开发板例程。
* [web](./web)  esp32 web例程目录。
* [ws_echo_server](./ws_echo_server) websocket server 移植于esp-idf ws_echo_server。本身已经包含http server无需再使用额外的http服务。


* [esp_now_to_linux](./esp_now_to_linux) esp32 通过espnow遥控 linux设备

* [s3Rgb](./s3Rgb) ESP32-S3-DevKitC-1 板载RGB灯示例,web控制rgb颜色

* [cam_ai](./cam_ai) ESP32-S3-WROOM CAM + ST7789 人脸检测+识别

* [idf_mpu6050](./idf_mpu6050) 使用idf组件库里的MPU6050库,读取加速度，陀螺仪参数和温度的例程。

* [i2s_recorder](./i2s_recorder) ESP32-S3-WROOM CAM +  INMP441 i2s标准模式 录音到sd卡。


* [80211raw](./80211raw)  esp32 对 esp32 使用80211帧通讯。

  类似于espnow无需建立sta ap 连接场景， 直接对空中发包和接受空中wifi帧来通讯的方式。

  但相比于espnow可以有更大的报文长度和更高的带宽。无需使用真实mac地址，可以事先约定源和目标地址，结合编写适当过滤规则即可通讯。

  同时espnow本质也是一种特定的80211中的帧。

* [algorithm](./algorithm) 算法，主要是安全相关的算法库使用，包括哈希，加密，和密钥协商算法。
  * [encryption](./algorithm/encryption) 加密算法 
  * [hash](./algorithm/hash) 哈希算法 MD5 /SHA256

* [ffmpeg](./ffmpeg) esp32推送音频或视频与ffmpeg交互的例子。

  esp32通过udp推送jpeg帧给ffmpeg,ffmpeg以h.264编码后以rtp推流，再使用vlc等播放器播放rtp流。
  
  esp32录音推送PCM数据给ffplay播放。


* [data_time](./date_time) 日期和时间，esp32和esp8266使用ntp对时成功后并本地获取时间格式化打印。

* [mpu6050_view](./mpu6050_view) esp32作为web服务端，提供web通过3d视图显示mpu6050的状态

  ![6050 3d](./mpu6050_view/demo.png)

 

* [newPio](./newPio) 在PIO使用新版的arduino平台(目前3.0.1) 支持esp32c6和esp32h2等，该项目未配置esp32h2可以参考esp32c6的配置对照
* [audio](./audio) esp32 i2s音频收发例子,使用i2s麦克风INMP441 和i2s功放 传输音频
* [dual_camera](./dual_camera) esp32s3驱动两个摄像头
* [rmt_pwm](./rmt_pwm) 使用rmt （红外遥控）外设生成pwm驱动舵机
* [rmt_rgb](./rmt_rgb) 使用rmt （红外遥控）外设驱动rgb灯条灯板（w2812等）
* [rgb_web](./rgb_web) 通过web控制使用rmt （红外遥控）外设驱动rgb灯条灯板（w2812等）
* [rmt_servo_lib](./rmt_servo_lib)  使用rmt 外设生成pwm驱动舵机，封装了 RmtServo类，  类似ESP32Servo 提供了 attach write 等函数。

* [usb](./usb) esp32s2/esp32s3 otg外设相关例程
