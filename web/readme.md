# web

web例程



[static_web](./static_web) esp32 使用静态html文件的web服务，这里使用flash存储静态web。



[esp32Vue](./esp32Vue) platformio 集成esp32 整合 vue 项目示例这里使用esp32的flash存储静态资源。

需要先安装nodejs npm环境，npm根据需要配置镜像。

将一个vue 3.0 hello world项目 集成，可以通过platformio 一键构建 vue项目，并将生成文件写入esp32 flash。

esp32作为服务端正确发送打包好的项目给浏览器。



[esp32WebSystemInfo](./esp32WebSystemInfo) platformio 集成esp32 整合 vue 项目示例，含静态页面和交互控制。

通过web获取esp32信息，和控制esp32 板载led示例。

在一个项目基础上整合 UI框架，[**arco design**]([Arco Design Vue](https://arco.design/vue/docs/start))