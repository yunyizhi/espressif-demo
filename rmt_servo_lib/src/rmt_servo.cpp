//
// Created by yunyizhi on 2024/12/1.
//

#include <Arduino.h>
#include "rmt_servo.h"

RmtServo::RmtServo()
{
    for (auto &item: pwm_) {
        item.level0 = 1;
        item.level1 = 0;
    }
}

int RmtServo::attach(int pin, uint32_t min, uint32_t max)
{
    log_i("init rmt tx");

#if ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5, 0, 0)
    if ((rmtSend_ = rmtInit(pin, RMT_TX_MODE, RMT_MEM_64)) == nullptr) {
        log_e("RMT init failed");
        rmtSend_ = nullptr;
        return false;
    }
    rmtSetTick(rmtSend_, 1000); // 设置为1Mhz即1000 ns为一个tick
#else

    if (!rmtInit(pin, RMT_TX_MODE, RMT_MEM_NUM_BLOCKS_1, 1000000)) { // 设置为1Mhz
        log_e("RMT init failed");
        return false;
    }
#endif

    log_i("rmt initialized");
    this->minPulseWidth_ = min;
    this->maxPulseWidth_ = max;
    this->pulseWidthRange_ = maxPulseWidth_ - minPulseWidth_;
    this->pin_ = pin;
    log_i("minPulseWidth:%lu,maxPulseWidth:%lu", min, max);
    return true;
}

void RmtServo::write(int value)
{

    if (value < 0) {
        value = 0;
    }

    if (value > MAX_RMT_SERVO_ANGLE) {
        value = MAX_RMT_SERVO_ANGLE;
    }

    uint32_t pulse_width_us =
            minPulseWidth_ + ((value * pulseWidthRange_) / MAX_RMT_SERVO_ANGLE);  // 0 ~ 180 -> min  ~ max
    for (auto &item: pwm_) {
        item.duration0 = pulse_width_us;
        item.duration1 = SERVO_PERIOD - pulse_width_us;
    }

#if ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5, 0, 0)
    rmtWriteBlocking(rmtSend_, pwm_, RMT_SERVO_TICKERS);
#else
    rmtWrite(pin_, pwm_, RMT_SERVO_TICKERS, RMT_WAIT_FOR_EVER);
#endif

}


void RmtServo::writeMicroseconds(int value)
{
    if (value < 0) {
        value = 0;
    }
    uint32_t value_ = value;
    if (value > maxPulseWidth_) {
        value_ = maxPulseWidth_;
    }
    for (auto &item: pwm_) {
        item.duration0 = value_;
        item.duration1 = SERVO_PERIOD - value_;
    }
#if ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5, 0, 0)
    rmtWriteBlocking(rmtSend_, pwm_, RMT_SERVO_TICKERS);
#else
    rmtWrite(pin_, pwm_, RMT_SERVO_TICKERS, RMT_WAIT_FOR_EVER);
#endif

}


