#include <Arduino.h>
#include "rmt_servo.h"

RmtServo servo;

void setup() {
    if (!servo.attach(GPIO_NUM_9)) {
        log_e("will reboot in 3 sec ");
        delay(3000);
        ESP.restart();
    }
}

void loop() {
    servo.write(0);
    delay(1000);
    servo.write(90);
    delay(1000);
    servo.write(180);
    delay(1000);
}