//
// Created by yunyizhi on 2024/12/1.
//

#ifndef RMT_SERVO_LIB_RMT_SERVO_H
#define RMT_SERVO_LIB_RMT_SERVO_H

// 50hz 周期为 20 ms
#define SERVO_PERIOD 20000

#ifndef MAX_RMT_SERVO_ANGLE
#define MAX_RMT_SERVO_ANGLE  180
#endif

#define RMT_SERVO_TICKERS 3

class RmtServo {
public:
    RmtServo();

    /**
     * @param pin 引脚 这里不检查输入范围，但rmt可能会初始化错误，请参考芯片手册rmt外设相关引脚
     * @param min 脉宽范围最小值 默认500微秒
     * @param max 脉宽范围最大值 默认2500
     * */
    int attach(int pin, uint32_t min = 500, uint32_t max = 2500);

    /**
     * 写入角度
     * @param value 0 ~ 180 会自动收敛到边界值
     * @throw 如果没有attach会导致rmt报错
     * */
    void write(int value);

    /**
     * 直接写高电平脉，实现更加精细角度控制<br>
     * 一般舵机可以输入500 ~ 2500 微秒的脉宽
     * @param value  单位微秒
     * @see #attach
     * 一般舵机范围 500 ~ 2500 在attach时指定
     * @throw 如果没有attach会导致rmt报错
     * */
    void writeMicroseconds(int value);

private:

    uint32_t minPulseWidth_{500};
    uint32_t maxPulseWidth_{2500};
    uint32_t pulseWidthRange_{2000};
    int pin_{-1};
#if ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5, 0, 0)
    rmt_obj_t *rmtSend_ = nullptr;
#endif
    rmt_data_t pwm_[RMT_SERVO_TICKERS]{};

};


#endif //RMT_SERVO_LIB_RMT_SERVO_H
