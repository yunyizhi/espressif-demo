//
// Created by immor on 2025/2/8.
//

#ifndef USB_MSC_H
#define USB_MSC_H
#include <Arduino.h>
#include <usb/msc_host.h>
#include <usb/msc_host_vfs.h>

extern "C" {
typedef struct {
    enum STATUS {
        APP_NOT_INITIALIZED = -1,
        APP_QUIT, // Signals request to exit the application
        APP_DEVICE_CONNECTED, // USB device connect event
        APP_DEVICE_DISCONNECTED, // USB device disconnect event
    } id;

    union {
        uint8_t new_dev_address; // Address of new USB device for APP_DEVICE_CONNECTED event if
    } data;
} Msc_Message_t;
}


class UsbMsc {
public:
    void init(gpio_num_t app_quit_pin,const char* mnt_path);
    Msc_Message_t::STATUS checkMscStatus(TickType_t xTicksToWait);
private:
    static void msc_event_cb(const msc_host_event_t *event, void *arg);

    static void usb_task(void *args);

    static void gpio_cb(void *arg);

    QueueHandle_t app_queue = nullptr;
    msc_host_device_handle_t msc_device = nullptr;
    msc_host_vfs_handle_t vfs_handle = nullptr;
    gpio_num_t app_quit_pin_ = GPIO_NUM_NC;
    char mnt_path_[32] {};
    Msc_Message_t::STATUS status_ {Msc_Message_t::APP_NOT_INITIALIZED};
};





#endif // USB_MSC_H
