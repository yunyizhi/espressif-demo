//
// Created by immor on 2025/2/8.
//

#ifndef DATETOOL_H
#define DATETOOL_H
#include <Arduino.h>


class DateTool {
  public:
    void configNtp(int timezone, int daylightOffset_sec, const char* server1,
    const char* server2 = nullptr, const char* server3 = nullptr);
    String getDateTime() const;
    tm getTime() const;
    private:
    bool syncOnce { false};
};



#endif //DATETOOL_H
