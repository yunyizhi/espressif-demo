//
// Created by immor on 2025/2/8.
//

#include "UsbMsc.h"

#include <sys/dirent.h>
#include <usb/usb_host.h>

void print_device_info(msc_host_device_info_t *info)
{
    constexpr size_t megabyte = 1024 * 1024;
    const uint64_t capacity = (static_cast<uint64_t>(info->sector_size) * info->sector_count) / megabyte;

    printf("Device info:\n");
    printf("\t Capacity: %llu MB\n", capacity);
    printf("\t Sector size: %" PRIu32 "\n", info->sector_size);
    printf("\t Sector count: %" PRIu32 "\n", info->sector_count);
    printf("\t PID: 0x%04X \n", info->idProduct);
    printf("\t VID: 0x%04X \n", info->idVendor);
    printf("\t iProduct: %ls \n", info->iProduct);
    printf("\t iManufacturer: %ls \n", info->iManufacturer);
    printf("\t iSerialNumber: %ls \n", info->iSerialNumber);
}

void UsbMsc::msc_event_cb(const msc_host_event_t *event, void *arg)
{
    const auto *usbMsc = static_cast<UsbMsc *>(arg);
    if (event->event == msc_host_event_t::MSC_DEVICE_CONNECTED) {
        log_i("MSC device connected");
        Msc_Message_t message = {
            Msc_Message_t::APP_DEVICE_CONNECTED,
            event->device.address,
        };
        xQueueSend(usbMsc->app_queue, &message, portMAX_DELAY);
        return;
    }
    if (event->event == msc_host_event_t::MSC_DEVICE_DISCONNECTED) {
        log_i("MSC device disconnected");
        constexpr Msc_Message_t message = {
            Msc_Message_t::APP_DEVICE_DISCONNECTED,
        };
        xQueueSend(usbMsc->app_queue, &message, portMAX_DELAY);
    }
}

void UsbMsc::usb_task(void *args)
{
    constexpr usb_host_config_t host_config = {.intr_flags = ESP_INTR_FLAG_LEVEL1};
    ESP_ERROR_CHECK(usb_host_install(&host_config));

    const msc_host_driver_config_t msc_config = {
        .create_backround_task = true,
        .task_priority = 5,
        .stack_size = 4096,
        .callback = msc_event_cb,
        .callback_arg = args,
    };
    ESP_ERROR_CHECK(msc_host_install(&msc_config));

    while (true) {
        uint32_t event_flags;
        usb_host_lib_handle_events(portMAX_DELAY, &event_flags);

        if (event_flags & USB_HOST_LIB_EVENT_FLAGS_NO_CLIENTS) {
            if (usb_host_device_free_all() == ESP_OK) {
                break;
            };
        }
        if (event_flags & USB_HOST_LIB_EVENT_FLAGS_ALL_FREE) {
            break;
        }
    }

    vTaskDelay(10); // Give clients some time to uninstall
    log_i("Deinitializing USB");
    ESP_ERROR_CHECK(usb_host_uninstall());
    vTaskDelete(nullptr);
}

void UsbMsc::gpio_cb(void *arg)
{
    const auto *usbMsc = static_cast<UsbMsc *>(arg);
    BaseType_t xTaskWoken = pdFALSE;
    constexpr Msc_Message_t message = {
        .id = Msc_Message_t::APP_QUIT,
    };

    if (usbMsc->app_queue) {
        xQueueSendFromISR(usbMsc->app_queue, &message, &xTaskWoken);
    }

    if (xTaskWoken == pdTRUE) {
        portYIELD_FROM_ISR();
    }
}

void UsbMsc::init(const gpio_num_t app_quit_pin, const char *mnt_path)
{
    strcpy(mnt_path_, mnt_path);
    app_queue = xQueueCreate(5, sizeof(Msc_Message_t));
    assert(app_queue);

    const BaseType_t task_created = xTaskCreate(usb_task, "usb_task", 4096, this, 2, NULL);
    assert(task_created);

    const gpio_config_t input_pin = {
        .pin_bit_mask = BIT64(app_quit_pin),
        .mode = GPIO_MODE_INPUT,
        .pull_up_en = GPIO_PULLUP_ENABLE,
        .intr_type = GPIO_INTR_NEGEDGE,
    };
    ESP_ERROR_CHECK(gpio_config(&input_pin));
    ESP_ERROR_CHECK(gpio_install_isr_service(ESP_INTR_FLAG_LEVEL1));
    ESP_ERROR_CHECK(gpio_isr_handler_add(app_quit_pin, gpio_cb, this));

    log_i("Waiting for USB flash drive to be connected");
}

Msc_Message_t::STATUS UsbMsc::checkMscStatus(const TickType_t xTicksToWait)
{
    Msc_Message_t msg;
    if (xQueueReceive(app_queue, &msg, xTicksToWait) != pdTRUE) {
        return status_;
    }
    status_ = msg.id;
    if (msg.id == Msc_Message_t::APP_DEVICE_CONNECTED) {
        ESP_ERROR_CHECK(msc_host_install_device(msg.data.new_dev_address, &msc_device));
        constexpr esp_vfs_fat_mount_config_t mount_config = {
            .format_if_mount_failed = false,
            .max_files = 3,
            .allocation_unit_size = 8192,
        };
        ESP_ERROR_CHECK(msc_host_vfs_register(msc_device, this->mnt_path_, &mount_config, &vfs_handle));

        msc_host_device_info_t info;
        ESP_ERROR_CHECK(msc_host_get_device_info(msc_device, &info));
        msc_host_print_descriptors(msc_device);
        print_device_info(&info);

        log_i("ls command output:");
        dirent *d;
        DIR *dh = opendir(this->mnt_path_);
        assert(dh);
        while ((d = readdir(dh)) != nullptr) {
            printf("%s\n", d->d_name);
        }
        closedir(dh);
    }
    if ((msg.id == Msc_Message_t::APP_DEVICE_DISCONNECTED) || (msg.id == Msc_Message_t::APP_QUIT)) {
        if (vfs_handle) {
            ESP_ERROR_CHECK(msc_host_vfs_unregister(vfs_handle));
            vfs_handle = nullptr;
        }
        if (msc_device) {
            ESP_ERROR_CHECK(msc_host_uninstall_device(msc_device));
            msc_device = nullptr;
        }
        if (msg.id == Msc_Message_t::APP_QUIT) {
            // This will cause the usb_task to exit
            ESP_ERROR_CHECK(msc_host_uninstall());
        }
    }
    return status_;
}
