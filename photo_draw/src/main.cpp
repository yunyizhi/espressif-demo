#include <Arduino.h>
#include <DateTool.h>
#include <sys/dirent.h>
#include <WiFi.h>
#include "usb/usb_host.h"
#include "usb/msc_host_vfs.h"
#include "esp_camera.h"
#include "UsbMsc.h"
#include "fb_gfx.h"
#include "jpeg/jpeg.h"

#define FACE_COLOR_WHITE 0x00FFFFFF
#define FACE_COLOR_BLACK 0x00000000
#define FACE_COLOR_RED 0x000000FF
#define FACE_COLOR_GREEN 0x0000FF00
#define FACE_COLOR_BLUE 0x00FF0000
#define FACE_COLOR_YELLOW (FACE_COLOR_RED | FACE_COLOR_GREEN)
#define FACE_COLOR_CYAN (FACE_COLOR_BLUE | FACE_COLOR_GREEN)
#define FACE_COLOR_PURPLE (FACE_COLOR_BLUE | FACE_COLOR_RED)

const char *ssid = WIFI_SSID;
const char *password = WIFI_PASSWORD;

constexpr long gmtOffset_sec = 28800; // 中国时区偏移量，UTC+8，即28800秒
constexpr int daylightOffset_sec = 0; // 夏令时偏移，中国不使用夏令时，设为0
const char *ntpServer = "ntp.aliyun.com";

const char *mnt_path = "/usb";
constexpr gpio_num_t msc_quit_pin = GPIO_NUM_0;

#define JPEG_BUF_SIZE  (500 * 1024)
#define JPEG_QUALITY  80
#define IMAGE_WIDTH_BYTES (1920 * 3)


uint8_t *jpegBuffer;
extern uint8_t *lineBuffer;

extern ssize_t rawFrame2jpg(camera_fb_t *fb, uint8_t quality, uint8_t *out, size_t out_len);

UsbMsc usb_msc;
DateTool date_tool;

static void rgb_print(fb_data_t *fb, uint32_t color, const char *str)
{
    fb_gfx_print(fb, (fb->width - (strlen(str) * 14)) / 2, 10, color, str);
}

static int rgb_printf(fb_data_t *fb, uint32_t color, const char *format, ...)
{
    char loc_buf[64];
    char *temp = loc_buf;
    int len;
    va_list arg;
    va_list copy;
    va_start(arg, format);
    va_copy(copy, arg);
    len = vsnprintf(loc_buf, sizeof(loc_buf), format, arg);
    va_end(copy);
    if (len >= sizeof(loc_buf)) {
        temp = (char *) malloc(len + 1);
        if (temp == NULL) {
            return 0;
        }
    }
    vsnprintf(temp, len + 1, format, arg);
    va_end(arg);
    rgb_print(fb, color, temp);
    if (len > 64) {
        free(temp);
    }
    return len;
}


void connectWifi()
{
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    // wait for WiFi connection
    Serial.println("Waiting for WiFi to connect...");
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println(" connected");
}

void setup()
{
    if (!psramFound()) {
        log_e("PSRAM not found, will restart in 3 sec");
        delay(3000);
        ESP.restart();
    }
    jpegBuffer = static_cast<uint8_t *>(ps_malloc(JPEG_BUF_SIZE));
    lineBuffer = static_cast<uint8_t *>(ps_malloc(IMAGE_WIDTH_BYTES));
    camera_config_t config;
    config.ledc_channel = LEDC_CHANNEL_0;
    config.ledc_timer = LEDC_TIMER_0;
    config.pin_d0 = Y2_GPIO_NUM;
    config.pin_d1 = Y3_GPIO_NUM;
    config.pin_d2 = Y4_GPIO_NUM;
    config.pin_d3 = Y5_GPIO_NUM;
    config.pin_d4 = Y6_GPIO_NUM;
    config.pin_d5 = Y7_GPIO_NUM;
    config.pin_d6 = Y8_GPIO_NUM;
    config.pin_d7 = Y9_GPIO_NUM;
    config.pin_xclk = XCLK_GPIO_NUM;
    config.pin_pclk = PCLK_GPIO_NUM;
    config.pin_vsync = VSYNC_GPIO_NUM;
    config.pin_href = HREF_GPIO_NUM;
    config.pin_sccb_sda = SIOD_GPIO_NUM;
    config.pin_sccb_scl = SIOC_GPIO_NUM;
    config.pin_pwdn = PWDN_GPIO_NUM;
    config.pin_reset = RESET_GPIO_NUM;
    config.xclk_freq_hz = 20000000;
    config.pixel_format = PIXFORMAT_RGB565;
    config.frame_size = FRAMESIZE_UXGA;
    config.fb_count = 1;


    // camera init
    esp_err_t err = esp_camera_init(&config);
    if (err != ESP_OK) {
        log_i("Camera init failed with error 0x%x", err);
        delay(3000);
        ESP.restart();
        return;
    }
    log_i("get sensor ");
    sensor_t *s = esp_camera_sensor_get();
    // drop down frame size for higher initial frame rate
    s->set_framesize(s, FRAMESIZE_UXGA);
    connectWifi();
    date_tool.configNtp(gmtOffset_sec, daylightOffset_sec, ntpServer);
    usb_msc.init(msc_quit_pin, mnt_path);
}

void loop()
{
    if (usb_msc.checkMscStatus(0) != Msc_Message_t::APP_DEVICE_CONNECTED) {
        delay(500);
        return;
    }
    const auto directory = "/usb/esp";
    struct stat s = {0};
    const bool directory_exists = stat(directory, &s) == 0;
    if (!directory_exists) {
        if (mkdir(directory, 0775) != 0) {
            log_e("mkdir failed with errno: %s", strerror(errno));
        }
    }
    camera_fb_t *fb = nullptr;
#define FILE_PATH_MAX_LEN 256 // 假设最长的文件路径不会超过256个字符
    fb_data_t rfb;
    char fileDir[FILE_PATH_MAX_LEN];
    fb = esp_camera_fb_get();
    if (!fb) {
        log_e("Camera capture failed");
        return;
    }
    rfb.width = fb->width;
    rfb.height = fb->height;
    rfb.data = fb->buf;
    rfb.bytes_per_pixel = 2;
    rfb.format = FB_RGB565;
    String dateTime = date_tool.getDateTime();
    rgb_print(&rfb, FACE_COLOR_WHITE, dateTime.c_str());
    ssize_t jpegLen = rawFrame2jpg(fb, JPEG_QUALITY, jpegBuffer, JPEG_BUF_SIZE);
    if (jpegLen < 0) {
        log_e("Failed to encode jpg");
        esp_camera_fb_return(fb);
        return;
    }
    const int64_t time = fb->timestamp.tv_sec * 1000LL + fb->timestamp.tv_usec / 1000LL; // 计算时间戳为毫秒
    snprintf(fileDir, sizeof(fileDir), "/usb/esp/%lld.jpg", time);
    FILE *f = fopen(fileDir, "wb+");
    if (f == nullptr) {
        log_e("Failed to open file for writing %s", strerror(errno));
        esp_camera_fb_return(fb);
        return;
    }
    setvbuf(f, nullptr, _IOFBF, jpegLen);
    if (fwrite(jpegBuffer, jpegLen, 1, f) == 0) {
        fclose(f);
        esp_camera_fb_return(fb);
        return;
    }
    fclose(f);
    esp_camera_fb_return(fb);
}
